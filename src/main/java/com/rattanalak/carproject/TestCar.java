/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.rattanalak.carproject;

/**
 *
 * @author Rattanalak
 */
public class TestCar {
    public static void main(String[] args) {
        Car car1 = new Car("Yellow", "MG", 1500, 115);
        car1.printCar();
        car1.setSpeed(120);
        car1.printCar();
        car1.newLine();
        Car car2 = new Car("White", "Toyota", 1500, 95);
        car2.printCar();
        car2.setSpeed(-10);
        car2.printCar();
        car2.setSpeed(140);
        car2.printCar();
        car2.newLine();
        Car car3 = new Car("Red", "Honda",1800,150);
        car3.printCar();
        car3.setSpeed(200);
        car3.printCar();
        car3.setSpeed(170);
        car3.printCar();
        car3.newLine();
        Car car4 = new Car("Blue", "Nissan",3700,180);
        car4.printCar();
        car4.setSpeed(210);
        car4.printCar();
        car4.setSpeed(160);
        car4.printCar();
        car4.newLine();
    }
}
