/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.rattanalak.carproject;

/**
 *
 * @author Rattanalak
 */
public class Car {

    private String color;
    private String brand;
    private int engine;
    private int speed;

    public Car(String color, String brand, int engine, int speed) {
        this.color = color;
        this.brand = brand;
        this.engine = engine;
        this.speed = speed;
    }

    public void setSpeed(int speed) {
        if (speed < 0) {
            System.out.println("Error : speed must higher than 0 !!");
            return;
        } else if (speed >= 200 && engine <= 2000) {
            System.out.println("Danger : Speed should not higher than 200!!");
            return;
        }else if(speed >= 200 && engine > 2000){
            System.out.println("Warning : Speed higher than 200 is Dangerous!!");
        }
        this.speed = speed;
    }

    public void printCar() {
        System.out.println(this.color + " " + this.brand + " " + "Car engine = "
                + this.engine + "cc.is driving with speed =" + this.speed + " km/hr");
    }

    public void newLine() {
        System.out.println("------------------------------------------------------------------");
    }

}
